get_filename_component(ProjectId ${CMAKE_SOURCE_DIR} NAME)
string(REPLACE " " "_" ProjectId ${ProjectId})
project(${ProjectId})
cmake_minimum_required(VERSION 2.8)
include_directories ("${PROJECT_SOURCE_DIR}/framework")
add_subdirectory ("${PROJECT_SOURCE_DIR}/framework")
include_directories ("${PROJECT_SOURCE_DIR}/solution")
add_subdirectory ("${PROJECT_SOURCE_DIR}/solution")
include_directories ("${PROJECT_SOURCE_DIR}/gui")
add_subdirectory ("${PROJECT_SOURCE_DIR}/gui")
find_package(PkgConfig)
pkg_check_modules(GTKMM gtkmm-3.0)
link_directories(${GTKMM_LIBRARY_DIRS})
include_directories(${GTKMM_INCLUDE_DIRS})
aux_source_directory(. SRC_LIST)
add_executable(${PROJECT_NAME} ${SRC_LIST})
ADD_DEFINITIONS("-std=c++0x -fpermissive -Ofast")
target_link_libraries( ${PROJECT_NAME} stdc++ pthread boost_thread boost_system Framework Solution Gui ${GTKMM_LIBRARIES})
file(GLOB libs_SRC "*.h" "*.hpp")
add_custom_target(libs SOURCES ${libs_SRC})






