#include "Factory.hpp"
#include "framework/OutgoingConnection.hpp"
#include "framework/IncomingConnection.hpp"
#include "framework/MessageSender.hpp"
#include "framework/TerminalLogger.hpp"
#include "framework/InternalQueue.hpp"
#include "framework/InternalMessageSender.hpp"
#include "solution/MainProgram.hpp"
#include "gui/MainWindow.hpp"

std::shared_ptr<gui::MainWindow> Factory::mainWindow;
std::shared_ptr<framework::InternalQueue> Factory::internalQueue;

std::unique_ptr<solution::MainProgram> Factory::createMainProgram(int otherPort)
{
    std::unique_ptr<solution::MainProgram> mainProgram(new solution::MainProgram(getInternalQueue(),
                                                                                 createMessageSender(otherPort),
                                                                                 getLogger(),
                                                                                 createMainWindow()));

    return mainProgram;
}

std::shared_ptr<framework::ILogger> Factory::getLogger()
{
    std::shared_ptr<framework::ILogger> logger = std::make_shared<framework::TerminalLogger>();
    return logger;
}

std::unique_ptr<framework::IMessageSender> Factory::createMessageSender(int port)
{
    std::shared_ptr<framework::IOutgoingConnection> connection = std::make_shared<framework::OutgoingConnection>("localhost", port);

    std::unique_ptr<framework::IMessageSender> messageSender (new framework::MessageSender {connection});

    return messageSender;
}

std::shared_ptr<gui::MainWindow> Factory::createMainWindow()
{
    if (not mainWindow)
    {
        std::shared_ptr<framework::InternalMessageSender> loopback = std::make_shared<framework::InternalMessageSender>(getInternalQueue());
        mainWindow = std::make_shared<gui::MainWindow>(loopback);
    }
    return mainWindow;
}

std::shared_ptr<framework::IncomingConnection> Factory::createIncomingConnection(int port)
{
    return std::make_shared<framework::IncomingConnection> (port, getInternalQueue());
}

std::shared_ptr<framework::InternalQueue> Factory::getInternalQueue()
{
    if (not internalQueue)
    {
        internalQueue = std::make_shared<framework::InternalQueue>();
    }
    return internalQueue;
}
