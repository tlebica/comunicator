#pragma once

#include <memory>

namespace solution
{
struct MainProgram;
}
namespace framework
{
struct IncomingConnection;
struct KeyboardReader;
struct IUserOutput;
struct ILogger;
struct IMessageSender;
struct InternalQueue;
}
namespace gui
{
struct MainWindow;
}

class Factory
{
public:
    static std::unique_ptr<solution::MainProgram> createMainProgram(int otherPort);
    static std::shared_ptr<gui::MainWindow> createMainWindow();
    static std::shared_ptr<framework::IncomingConnection> createIncomingConnection(int port);

private:
    static std::shared_ptr<framework::ILogger> getLogger();
    static std::unique_ptr<framework::IMessageSender> createMessageSender(int port);
    static std::shared_ptr<framework::InternalQueue> getInternalQueue();
    static std::shared_ptr<gui::MainWindow> mainWindow;
    static std::shared_ptr<framework::InternalQueue> internalQueue;
};
