#pragma once

#include <string>

namespace framework
{
class IInternalMessageSender
{
public:
    virtual void sendExitMessage() = 0;
    virtual void sendTextMessage(std::string& text) = 0;
    virtual void sendSetReceipent(std::string& name) = 0;
    virtual void sendSetReceipentToAll() = 0;
};
}
