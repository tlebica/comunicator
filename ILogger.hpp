#pragma once

#include <string>

namespace framework
{

class ILogger
{
public:
    virtual void logError(std::string&) = 0;
    virtual void logWarrning(std::string&) = 0;
    virtual void logInfo(std::string&) = 0;
    virtual ~ILogger(){}
};
}
