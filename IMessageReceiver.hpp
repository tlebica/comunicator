#pragma once

namespace framework
{
class IMessageReceiver
{
public:
    virtual const char * receive() = 0;
};

}
