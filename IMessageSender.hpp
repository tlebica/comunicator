#pragma once

#include <string>
#include <set>

namespace framework
{
class IMessageSender
{
public:
    virtual void sendTo(int address, int size, const char * message) = 0;
    virtual const std::set<int>& getAddresses() = 0;
};

}
