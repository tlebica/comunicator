#pragma once
#include <string>

namespace framework
{
class IUserOutput
{
public:
    virtual void print(std::string& message) = 0;
    virtual ~IUserOutput(){}
};
}
