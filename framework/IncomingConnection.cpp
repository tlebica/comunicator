#include "IncomingConnection.hpp"
#include <netinet/ip.h>
#include <unistd.h>
#include <Utilities.hpp>
#include <MessageTypes.hpp>
#include <iostream>
#include <InternalQueue.hpp>
#include "OutgoingConnection.hpp"

namespace framework
{

void IncomingConnection::start()
{
    struct sockaddr_in socket_address = {};
    socket_address.sin_family = AF_INET;
    socket_address.sin_port = htons(port);
    socket_address.sin_addr.s_addr = htonl(INADDR_ANY);

    socketId = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    //if socketId == -1 throw
    bind(socketId, (const sockaddr*)&socket_address, sizeof(socket_address));
    //if bind returns -1 throw

    while (1)
    {
        const char * message = receive();
        if (running)
        {
            queue->send(message);
        }
        else
        {
            return;
        }
    }
}

void IncomingConnection::stop()
{
    running = false;
    OutgoingConnection connection ("localhost", port);
    std::string dummy = "dummy";
    connection.send(dummy);
}

const char * IncomingConnection::receive()
{
    char * data = new char[BUFFER_SIZE+1];
    sockaddr_in sender_address = {};
    socklen_t addressLength = sizeof(sender_address);

    size_t length = recvfrom(socketId, data + 1, BUFFER_SIZE, 0, (sockaddr*)&sender_address, &addressLength);

    data[0] = static_cast<char>(MessageType::EXTERNAL);
    data[length+1] = 0;
    return data;
}



IncomingConnection::IncomingConnection(int port, std::shared_ptr<InternalQueue> queue)
    : port(port),
      socketId(0),
      queue(queue),
      running(true)
{

}

IncomingConnection::~IncomingConnection()
{
    close(socketId);
}
}
