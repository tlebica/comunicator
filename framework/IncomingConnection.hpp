#pragma once

#include <string>
#include <memory>

namespace framework
{
struct InternalQueue;

class IncomingConnection
{
public:
    IncomingConnection(int port, std::shared_ptr<InternalQueue> queue);
    ~IncomingConnection();
    void start();
    void stop();
private:
    const char * receive();
    const int port;
    int socketId;
    std::shared_ptr<InternalQueue> queue;
    bool running;
    static const int BUFFER_SIZE = 2048;

};
}
