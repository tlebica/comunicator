#include "InternalMessageSender.hpp"
#include "../IOutgoingConnection.hpp"
#include "MessageTypes.hpp"
#include "InternalQueue.hpp"

namespace framework
{

void InternalMessageSender::sendExitMessage()
{
    Message msg = createExitMessage();

    char* buffer = (char*)&msg;

    queue->copyAndSend(sizeof(msg), toBytes(msg));
}

void InternalMessageSender::sendTextMessage(std::string& text)
{
    Message msg = createTextMessage(text);
    queue->copyAndSend(sizeof(msg), toBytes(msg));
}


void InternalMessageSender::sendSetReceipent(std::string& name)
{
}


void InternalMessageSender::sendSetReceipentToAll()
{
}

}
