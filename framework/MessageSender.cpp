#include "MessageSender.hpp"

namespace framework
{
void MessageSender::sendTo(int address, int size, const char * message)
{
    auto connectionIterator = connectionMap.find(address);
    if (connectionIterator != connectionMap.end())
    {
        connectionIterator->second->send(size, message);
    }
}

const std::set<int>& MessageSender::getAddresses()
{
    return addresses;
}

}

