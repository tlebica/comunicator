#pragma once

#include "../IMessageSender.hpp"
#include "../IOutgoingConnection.hpp"
#include <map>
#include <memory>
#include <initializer_list>
#include <utility>

namespace framework
{

using Connection = std::shared_ptr<IOutgoingConnection>;

class MessageSender : public IMessageSender
{
public:
    MessageSender(std::initializer_list<Connection> list)
        :connectionMap(),
         addresses()
    {
        for(auto& connection : list)
        {
            std::pair<int, Connection> pair(connection->getAddress(), connection);
            connectionMap.insert(pair);
            addresses.insert(connection->getAddress());
        }
    }
    void sendTo(int address, int size, const char * message) override;
    const std::set<int>& getAddresses() override;

private:
    std::map<int, Connection> connectionMap;
    std::set<int> addresses;

};
}
