#include "MessageTypes.hpp"
#include <algorithm>

namespace framework
{
Message createExitMessage()
{
    Message result {};
    result.messageType = static_cast<char>(MessageId::EXIT);
    return result;
}

Message createTextMessage(std::string& text)
{
    Message result {};
    result.messageType = static_cast<char>(MessageId::TEXT);
    int size = std::min(static_cast<int>(text.size()), MAX_TEXT_LENGHT);
    std::copy(text.begin(), text.begin() + size, result.data.text.data);
    result.data.text.length = static_cast<short>(size);
    return result;
}

char* toBytes(Message& msg)
{
    return reinterpret_cast<char*>(&msg);
}
}
