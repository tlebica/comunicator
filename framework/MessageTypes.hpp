#pragma once

#include <string>

namespace framework
{

const int MAX_TEXT_LENGHT = 1000;

enum class MessageType : char
{
    INTERNAL = 1,
    EXTERNAL = 2
};

enum class MessageId : short
{
    EXIT = 1,
    TEXT = 2,
    USER_INFO_RESPONSE = 3,
    USER_INFO_REQUEST = 4
};

struct UserInfoResponse
{
    short length;
    char data[1];
} __attribute__ ((__packed__));

struct TextMessage
{
    short length;
    char data[MAX_TEXT_LENGHT];
} __attribute__ ((__packed__));

union Data
{
    TextMessage text;
    UserInfoResponse userInfoResponse;
};

struct Message
{
    char messageType;
    Data data;
} __attribute__ ((__packed__));

Message createExitMessage();

Message createTextMessage(std::string& text);

char* toBytes(Message& msg);
}
