#pragma once

#include <string>
#include <netinet/ip.h>
#include <../IOutgoingConnection.hpp>

namespace framework
{

class OutgoingConnection : public IOutgoingConnection
{
public:
    OutgoingConnection(const std::string& address, int port);
    ~OutgoingConnection();
    void send (const std::string& message) override;
    void send(int size, const char *) override;
    int getAddress() override;
private:
    int socketId;
    sockaddr_in addressInfo;
};
}
