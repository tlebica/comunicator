#pragma once

#include "../ILogger.hpp"

namespace framework
{

class TerminalLogger : public ILogger
{
public:
    virtual void logError(std::string &);
    virtual void logWarrning(std::string &);
    virtual void logInfo(std::string &);
};
}
