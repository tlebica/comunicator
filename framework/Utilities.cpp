#include "Utilities.hpp"
#include <netdb.h>
#include <unistd.h>

namespace framework
{
int getHostAddress(const char * host)
{
    hostent * hostEntity = gethostbyname(host);
    return *((int*)hostEntity->h_addr);
}
}
