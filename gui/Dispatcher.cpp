#include "Dispatcher.hpp"

namespace gui
{
void Dispatcher::dispatch(Call call)
{
    this->call = call;
    dispatcher();
    {
       std::unique_lock<std::mutex> lock(mutex);
       conditional.wait(lock);
    }

}

void Dispatcher::handle()
{
    if (call)
        call();

    call = nullptr;

    conditional.notify_one();
}
}
