#include "MainWindow.hpp"

namespace
{
void removeEndLineChars(std::string& msg)
{
    int i = msg.size() - 1;
    while ((i >= 0) && (msg[i] == '\r' || msg[i] == '\n'))
    {
        --i;
    }
    msg.erase(i+1);
}
}

namespace gui
{
MainWindow::MainWindow(std::shared_ptr<framework::IInternalMessageSender> loopback)
    : mainBox(Gtk::ORIENTATION_VERTICAL),
      horizontalBox(Gtk::ORIENTATION_HORIZONTAL),
      hseparator1(Gtk::ORIENTATION_HORIZONTAL),
      hSeparator2(Gtk::ORIENTATION_HORIZONTAL),
      vSeparator(Gtk::ORIENTATION_VERTICAL),
      dispatcher(),
      clearBuffer(),
      loopback(loopback)
{
    set_title("Communicator");
    set_border_width(1);
    set_default_size(400, 200);

    add(mainBox);

    mainBox.pack_start(mainLabel);
    mainBox.pack_start(hseparator1);
    mainBox.pack_start(horizontalBox);
    mainBox.pack_start(hSeparator2);
    mainBox.pack_start(inputTextView);

    horizontalBox.pack_start(connectionList);
    horizontalBox.pack_start(vSeparator);
    horizontalBox.pack_start(outputTextView);
    horizontalBox.set_vexpand(true);
    horizontalBox.set_size_request(-1, 450);

    mainLabel.set_size_request(-1,75);
    mainLabel.set_vexpand(false);
    hseparator1.set_vexpand(false);
    hseparator1.property_expand().set_value(false);

    inputTextView.set_vexpand(false);
    inputTextView.set_size_request(-1,150);
    inputTextView.set_hexpand(false);

    inputTextView.get_buffer()->signal_changed().connect(sigc::mem_fun(*this, &MainWindow::handleInputChange),false);

    connectionList.set_hexpand(false);
    connectionList.set_vexpand(true);
    connectionList.set_size_request(225,-1);

    vSeparator.set_hexpand(false);
    vSeparator.set_size_request(5,-1);
    outputTextView.set_size_request(450,-1);
    outputTextView.property_editable().set_value(false);

    property_has_resize_grip().set_value(false);

    show_all_children();

    signal_delete_event().connect(sigc::mem_fun(*this, &MainWindow::handleClose), false);
    clearBuffer.connect(sigc::mem_fun(*this, &MainWindow::clearInput));
}

void MainWindow::print(std::string& message)
{
    dispatcher.dispatch([this, message]{outputTextView.get_buffer()->insert(outputTextView.get_buffer()->end(),message + "\n"); });
}

void MainWindow::handleInputChange()
{
    if (inputTextView.get_buffer()->get_line_count() > 1)
    {
        std::string msg = inputTextView.get_buffer()->get_text();
        removeEndLineChars(msg);
        loopback->sendTextMessage(msg);
        clearBuffer();
    }
}

bool MainWindow::handleClose(GdkEventAny *)
{
    loopback->sendExitMessage();
    return false;
}

void MainWindow::clearInput()
{
    inputTextView.get_buffer()->set_text("");
}

}
