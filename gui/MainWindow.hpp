#pragma once

#include <gtkmm.h>
#include <string>
#include <memory>
#include <Dispatcher.hpp>
#include "../IUserOutput.hpp"
#include "../IInternalMessageSender.hpp"

namespace gui
{
class MainWindow : public Gtk::Window, public framework::IUserOutput
{
public:
    MainWindow(std::shared_ptr<framework::IInternalMessageSender> loopback);
    void print(std::string& message);


private:
    void handleInputChange();
    bool handleClose(GdkEventAny*);
    void clearInput();
    Gtk::Box mainBox;
    Gtk::Box horizontalBox;
    Gtk::Label mainLabel;
    Gtk::Separator hseparator1;
    Gtk::Separator hSeparator2;
    Gtk::Separator vSeparator;
    Gtk::TextView inputTextView;
    Gtk::TreeView connectionList;

    Gtk::TextView outputTextView;
    Dispatcher dispatcher;
    Glib::Dispatcher clearBuffer;
    std::shared_ptr<framework::IInternalMessageSender> loopback;

};

}
