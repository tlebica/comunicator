#include <iostream>
#include <memory>
#include <thread>
#include <chrono>
#include <string>
#include <gtkmm.h>
#include "Factory.hpp"
#include "solution/MainProgram.hpp"
#include "gui/MainWindow.hpp"
#include "framework/IncomingConnection.hpp"
#include <stdlib.h>
#include <time.h>

std::string appId()
{
    return "communicator.nokia.a"+ std::to_string(rand()) + "a";
}

int main(int argc, char* argv[])
{
    srand(time(NULL));
    int otherPort = 8092;
    int port = 8093;

    if (argc > 1)
    {
       port = 8092;
       otherPort = 8093;
    }

    argc = 1;

    if(!Glib::thread_supported()) Glib::thread_init();

    Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(argc, argv, appId());
    auto window = Factory::createMainWindow();

    auto incomingConnection = Factory::createIncomingConnection(port);
    std::thread incomingThread {[&]{incomingConnection->start();}};

    auto mainProgram = Factory::createMainProgram(otherPort);
    std::thread controllerThread {[&]{mainProgram->run();}};

    app->run(*window);
    incomingConnection->stop();

    controllerThread.join();
    incomingThread.join();
}
