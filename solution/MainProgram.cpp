#include "MainProgram.hpp"

namespace solution
{
void MainProgram::run()
{
    while (1)
    {
        const char * message = receive();

        logError("Message received");
        logError(std::to_string(message[0]));
        logError(std::to_string(message[1]));

        if (message[0] == 1) //Internal
        {
            if (message[1] == 1) // Exit
            {
                delete [] message;
                return;
            }
            if (message[1] == 2) // Text
            {
                int size = ((short*)(message))[1];

                logError(std::to_string(size));

                std::string text {message+4, size};
                print( "[Me] " + text);
                send(size + 3, message + 1);
            }
        }
        if (message[0] == 2) //External
        {
            if (message[1] == 2) // Text
            {
                int size = ((short*)(message))[1];

                logError(std::to_string(size));

                std::string text {message+4, size};
                print( "[unknown] " + text);
            }
        }

        delete[] message;
    }
}
}
