#pragma once

#include <../IUserOutput.hpp>
#include <../ILogger.hpp>
#include <../IMessageSender.hpp>
#include <../IMessageReceiver.hpp>

#include <memory>

namespace solution
{
class MainProgram
{
public:
    MainProgram(std::shared_ptr<framework::IMessageReceiver> receiver,
                std::unique_ptr<framework::IMessageSender> sender,
                std::shared_ptr<framework::ILogger> logger,
                std::shared_ptr<framework::IUserOutput> userOutput)
        :receiver(receiver),
         messageSender(std::move(sender)),
         logger(logger),
         userOutput(userOutput)
    {}
    void run();
private:
    const char * receive()
    {
        return receiver->receive();
    }

    void send(int size, const char * message)
    {
        messageSender->sendTo(*(messageSender->getAddresses().begin()), size, message);
    }

    void sendTo(int address, int size, const char * message)
    {
        messageSender->sendTo(address, size, message);
    }

    void logError(std::string message)
    {
        logger->logError(message);
    }

    void print(std::string message)
    {
        userOutput->print(message);
    }

    std::shared_ptr<framework::IMessageReceiver> receiver;
    std::unique_ptr<framework::IMessageSender> messageSender;
    std::shared_ptr<framework::ILogger> logger;
    std::shared_ptr<framework::IUserOutput> userOutput;
};
}
